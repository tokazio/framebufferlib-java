package framebuffer.mid;

/**
 *
 * @author tokazio
 */
public interface Drawable {
    
    public void draw() throws Throwable;
    
    public void staticDraw() throws Throwable;
    
}
