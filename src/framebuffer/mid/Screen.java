package framebuffer.mid;

import framebuffer.low.FrameBufferLib;
import java.io.File;

/**
 *
 * @author tokazio
 */
public class Screen {
    
    /**
     * 
     */
    public static Screen screen;
    
    /**
     * 
     */
    public Screen(String device) throws ScreenException{
        if(device.isEmpty()){
            throw new ScreenException("Need a screen device path");
        }
        File f = new File(device);
        if(!f.exists()){
            throw new ScreenException("Screen device '"+device+"' doesn't exists");
        }            
        FrameBufferLib.fb_open(device);
        FrameBufferLib.text_open();
    }
    
    /**
     * 
     */
    public void printInfos(){
        FrameBufferLib.fb_print_info();
    }
    
    /**
     * 
     * @return 
     */
    public int getWidth(){
        return FrameBufferLib.fb_screen_width();
    }
    
    /**
     * 
     * @return 
     */
    public int getHeight(){
        return FrameBufferLib.fb_screen_height();
    }
    
    /**
     * 
     */
    public void clear(){
        FrameBufferLib.fb_clear_screen(0);
    }
    
    /**
     * 
     */
    public void flip(){
        FrameBufferLib.fb_page_flip();
    }
    
    /**
     * 
     * @throws Throwable 
     */
    @Override
    public void finalize() throws Throwable{
        try {
            FrameBufferLib.text_close();
            FrameBufferLib.fb_close();
        } finally {
            super.finalize();
        }
    }
    
}
