package framebuffer.mid;

/**
 *
 * @author tokazio
 */
public class ImageException extends Exception {

    public ImageException(String s) {
        super(s);
    }
    
}
