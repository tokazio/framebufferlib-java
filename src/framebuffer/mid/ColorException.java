package framebuffer.mid;

/**
 *
 * @author tokazio
 */
public class ColorException extends Exception {

    public ColorException(String s) {
        super(s);
    }
    
}
