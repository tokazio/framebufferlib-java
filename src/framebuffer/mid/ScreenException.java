package framebuffer.mid;

/**
 *
 * @author tokazio
 */
public class ScreenException extends Exception {

    public ScreenException(String s) {
        super(s);
    }
    
}
