package framebuffer.mid;

import framebuffer.low.FrameBufferLib;

/**
 *
 * @author tokazio
 */
public class Core {

    //draw
    /**
     *
     * @param x
     * @param y
     * @param c
     * @throws ColorException
     */
    public static void draw_pixel(int x, int y, Color c) throws ColorException {
        if (c == null || c.getPointer() == 0) {
            throw new ColorException("No color defined for Core.draw_pixel");
        }
        FrameBufferLib.fb_draw_pixel(x, y, c.getPointer());
    }

    /**
     *
     * @param x
     * @param y
     * @param w
     * @param h
     * @param c
     * @throws ColorException
     */
    public static void fill_rect(int x, int y, int w, int h, Color c) throws ColorException {
        if (c == null || c.getPointer() == 0) {
            throw new ColorException("No color defined for Core.fill_rect");
        }
        //System.out.println("fill rect: "+x+","+y+" "+w+","+h);
        FrameBufferLib.fb_fill_rect(x, y, w, h, c.getPointer());
    }

    /**
     *
     * @param c
     */
    public static void clear_screen(Color c) {
        if (c == null) {
            return;
        }
        FrameBufferLib.fb_clear_screen(c.getPointer());
    }

    /**
     *
     * @param x0
     * @param y0
     * @param r
     * @param c
     * @throws ColorException
     */
    public static void fill_circle(int x0, int y0, int r, Color c) throws ColorException {
        if (c == null || c.getPointer() == 0) {
            throw new ColorException("No color defined for Core.fill_circle");
        }
        //System.out.println("fill circle: "+x0+","+y0+","+r+" "+c.getPointer());
        //FrameBufferLib.fb_fill_circle(x0, y0, r, c.getPointer());
    }

    /**
     *
     * @param x0
     * @param y0
     * @param r
     * @param c
     * @throws ColorException
     */
    public static void stroke_circle(int x0, int y0, int r, Color c) throws ColorException {
        if (c == null || c.getPointer() == 0) {
            throw new ColorException("No color defined for Core.stroke_circle");
        }
        FrameBufferLib.fb_stroke_circle(x0, y0, r, c.getPointer());
    }

    /**
     *
     * @param x0
     * @param y0
     * @param x1
     * @param y1
     * @param c
     * @throws ColorException
     */
    public static void stroke_line(int x0, int y0, int x1, int y1, Color c) throws ColorException {
        if (c == null || c.getPointer() == 0) {
            throw new ColorException("No color defined for Core.stroke_line");
        }
        FrameBufferLib.fb_stroke_line(x0, y0, x1, y1, c.getPointer());
    }

    /**
     *
     * @param x
     * @param y
     * @param c
     * @param s
     * @throws ColorException
     */
    public static void fill(int x, int y, Color c, Color s) throws ColorException {
        if (c == null || c.getPointer() == 0) {
            throw new ColorException("No fill color defined for Core.fill");
        }
        if (s == null) {
            throw new ColorException("No background color defined for Core.fill");
        }
        FrameBufferLib.fb_fill(x, y, c.getPointer(), s.getPointer());
    }

    //jpeg
    /**
     *
     * @param image
     * @throws ImageException
     */
    public static void fill_jpg_at(Image image) throws ImageException {
        if (image == null || image.getPointer() == 0) {
            throw new ImageException("No image defined for Core.fill_jpg_at");
        }
        FrameBufferLib.fb_fill_jpg_at(image.getPointer(), image.getX(), image.getY());
    }

    /**
     *
     * @param image
     * @throws ImageException
     */
    public static void fill_jpg_at_size(Image image) throws ImageException {
        if (image == null || image.getPointer() == 0) {
            throw new ImageException("No image defined for Core.fill_jpg_at_size");
        }
        FrameBufferLib.fb_fill_jpg_at_size(image.getPointer(), image.getX(), image.getY(), image.getWidth(), image.getHeight());
    }

    /**
     *
     * @param image
     * @param xscale
     * @param yscale
     * @throws ImageException
     */
    public static void fill_jpg_at_scale(Image image, double xscale, double yscale) throws ImageException {
        if (image == null || image.getPointer() == 0) {
            throw new ImageException("No image defined for Core.fill_jpg_at_scale");
        }
        FrameBufferLib.fb_fill_jpg_at_scale(image.getPointer(), image.getX(), image.getY(), xscale, yscale);
    }

    /**
     *
     * @param image
     * @throws framebuffer.mid.ImageException
     */
    public static void jpg_image_(Image image) throws ImageException {
        if (image == null || image.getPointer() == 0) {
            throw new ImageException("No image defined for Core.jpg_image_");
        }
        FrameBufferLib.fb_jpg_image_(image.getPointer());

    }

    //text
    /**
     *
     * @param font
     * @throws framebuffer.mid.FontException
     */
    public static void close_font(Font font) throws FontException {
        if (font == null || font.getPointer() == 0) {
            throw new FontException("No image defined for Core.close_font");
        }
        FrameBufferLib.text_close_font(font.getPointer());

    }

    /**
     *
     * @param font
     * @param x
     * @param y
     * @param text
     * @param col
     * @throws FontException
     * @throws ColorException
     */
    public static void text_fill(Font font, int x, int y, String text, Color col) throws FontException, ColorException {
        if (font == null || font.getPointer() == 0) {
            throw new FontException("No font defined for Core.text_fill");
        }
        if (col == null || col.getPointer() == 0) {
            throw new ColorException("No color defined for Core.text_fill");
        }
        FrameBufferLib.text_fill(font.getPointer(), x, y, font.getSize(), text, col.getPointer());
    }

    /**
     *
     * @param font
     * @param text
     * @return
     * @throws FontException
     */
    public static int text_height(Font font, String text) throws FontException {
        if (font == null || font.getPointer() == 0) {
            throw new FontException("No font defined for Core.text_height");
        }
        return FrameBufferLib.text_height(font.getPointer(), font.getSize(), text);
    }

    /**
     *
     * @param font
     * @param text
     * @return
     * @throws FontException
     */
    public static int text_width(Font font, String text) throws FontException {
        if (font == null || font.getPointer() == 0) {
            throw new FontException("No font defined for Core.text_width");
        }
        return FrameBufferLib.text_width(font.getPointer(), font.getSize(), text);
    }

}
