package framebuffer.mid;

import framebuffer.components.DrawableComponent;
import framebuffer.low.FrameBufferLib;
import java.io.File;

/**
 *
 * @author tokazio
 */
public class Image extends DrawableComponent {

    /**
     *
     */
    private long img = 0;

    /**
     *
     * @param x
     * @param y
     * @param w
     * @param h
     * @param filename
     * @throws framebuffer.mid.ImageException
     */
    public Image(int x, int y, int w, int h, String filename) throws ImageException {
        super(x, y, w, h);
        File f = new File(filename);
        if (f.exists()) {
            this.img = FrameBufferLib.fb_jpg_image_load_file(filename);
        }else{
            throw new ImageException("Image file '"+filename+" doesn't exists");
        }
    }

    /**
     *
     * @throws ImageException
     */
    @Override
    public void draw() throws ImageException {
        Core.fill_jpg_at_size(this);
    }

    /**
     *
     * @throws ImageException
     */
    @Override
    public void staticDraw() throws ImageException {
        Core.fill_jpg_at_size(this);
    }

    /**
     *
     * @return
     */
    public long getPointer() {
        return this.img;
    }

    /**
     *
     * @throws Throwable
     */
    @Override
    public void finalize() throws Throwable {
        try {
            if (img == 0) {
                FrameBufferLib.fb_jpg_image_(img);
            }
        } finally {
            super.finalize();
        }
    }
}
