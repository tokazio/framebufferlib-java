package framebuffer.mid;

import framebuffer.low.FrameBufferLib;

/**
 *
 * @author tokazio
 */
public class Color {

    public static final Color EMPTY = new Color(0, 0, 0, 0);
    public static final Color BLACK = new Color(0, 0, 0, 255);
    public static final Color DARKGRAY = new Color(64, 64, 64, 255);
    public static final Color GRAY = new Color(128, 128, 128, 255);
    public static final Color LIGHTGRAY = new Color(192, 192, 192, 255);
    public static final Color WHITE = new Color(255, 255, 255, 255);
    public static final Color RED = new Color(255, 0, 0, 255);
    public static final Color GREEN = new Color(0, 255, 0, 255);
    public static final Color BLUE = new Color(0, 0, 255, 255);

    private short r;
    private short g;
    private short b;
    private short a;
    private long col;

    public Color(int r, int g, int b, int a) {
        this.r = (short) (r < 0 ? 0 : r > 255 ? 255 : r);
        this.g = (short) (g < 0 ? 0 : g > 255 ? 255 : g);
        this.b = (short) (b < 0 ? 0 : b > 255 ? 255 : b);
        this.a = (short) (a < 0 ? 0 : a > 255 ? 255 : a);
        this.col = FrameBufferLib.mapRgba(this.r, this.g, this.b, this.a);
    }

    public long getPointer() {
        return this.col;
    }

    public Color(int r, int g, int b) {
        this(r, g, b, 255);
    }

    @Override
    public void finalize() throws Throwable {
        try {
            FrameBufferLib.unmapRgba(col);
        } finally {
            super.finalize();
        }
    }
}
