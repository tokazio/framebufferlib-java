package framebuffer.mid;

import framebuffer.low.FrameBufferLib;
import java.io.File;

/**
 *
 * @author tokazio
 */
public class Font {

    /**
     *
     */
    private String fontFile;

    /**
     *
     */
    private int fontSize = 12;
    /**
     *
     */
    private long font = 0;

    /**
     *
     * @param filename
     * @param size
     * @throws framebuffer.mid.FontException
     */
    public Font(String filename, int size) throws FontException {
        this.fontFile = filename;
        this.fontSize = size;
        File f = new File(filename);
        if (f.exists()) {
            this.font = FrameBufferLib.text_open_font(fontFile);
        } else {
            throw new FontException("Font file '" + filename + "' doesn't exists");
        }

    }

    /**
     *
     * @param fontSize
     * @return
     */
    public Font setSize(int fontSize) {
        if (fontSize == this.fontSize) {
            return this;
        }
        this.fontSize = fontSize;
        return this;
    }

    /**
     *
     * @return
     */
    public int getSize() {
        return this.fontSize;
    }

    /**
     *
     * @return
     */
    public long getPointer() {
        return this.font;
    }

    /**
     *
     * @throws Throwable
     */
    @Override
    public void finalize() throws Throwable {
        try {
            if (font > 0) {
                FrameBufferLib.text_close_font(font);
            }
        } finally {
            super.finalize();
        }
    }

}
