package framebuffer.mid;

/**
 *
 * @author tokazio
 */
public class FontException extends Exception {

    public FontException(String s) {
        super(s);
    }
    
}
