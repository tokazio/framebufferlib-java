package framebuffer.mid;

/**
 *
 * @author tokazio
 */
public interface Clickable {
    
    public void onClick(int x, int y) throws Throwable;
    
}
