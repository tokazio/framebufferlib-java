package framebuffer.components;

import framebuffer.mid.Core;
import framebuffer.mid.Color;
import framebuffer.mid.ColorException;
import framebuffer.mid.FontException;

/**
 *
 * @author tokazio
 */
public class Button extends ClickableComponent{

    public Button(int x, int y, int w, int h, String caption) throws FontException {
        super(x, y, caption);
        this.setTextColor(Color.BLACK);
    }

    @Override
    public void draw() throws ColorException, FontException {
        Core.fill_rect(x, y, width, height, bgColor);   
        Core.text_fill(font,calcTextX(),calcTextY(),caption,textColor);
    }

    @Override
    public void staticDraw() throws ColorException, FontException {
        Core.fill_rect(x, y, width, height, bgColor);   
        Core.text_fill(font,calcTextX(),calcTextY(),caption,textColor);
    }

    @Override
    public void onClick(int x, int y) {
        System.out.println("Click on ? @"+x+","+y);
    }
        
}
