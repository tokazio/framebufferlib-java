package framebuffer.components;

import framebuffer.mid.Core;
import framebuffer.mid.Color;
import framebuffer.mid.ColorException;
import framebuffer.mid.FontException;

/**
 *
 * @author tokazio
 */
public class Label extends TextComponent {

    public Label(int x, int y, String caption) throws FontException {
        super(x, y, caption);        
    }

    @Override
    public void draw() throws ColorException, FontException {
        //@todo effacer avec une copie de l'arrière plan
        Core.fill_rect(x, y, width, height, Color.BLACK);
        //Core.text_fill(font,calcTextX(),calcTextY(),caption,textColor);        
    }

    @Override
    public void staticDraw() throws FontException, ColorException {
        //Core.text_fill(font,calcTextX(),calcTextY(),caption,textColor);
    }
    
}
