package framebuffer.components;

import framebuffer.mid.Drawable;
import framebuffer.mid.Color;

/**
 *
 * @author tokazio
 */
public abstract class DrawableComponent implements Drawable {
    
    protected int x;
    protected int y;
    protected int width = 0;
    protected int height = 0;
    protected Color bgColor = Color.BLACK;
    protected Color color = Color.WHITE;
    
    public DrawableComponent(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public DrawableComponent(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
    }
    
    public DrawableComponent setX(int x) {
        if(x==this.x){
            return this;
        }
        this.x = x;
        return this;
    }

    public DrawableComponent setY(int y) {
        if(y==this.y){
            return this;
        }
        this.y = y;
        return this;
    }

    public DrawableComponent setWidth(int width) {        
        width = width < 0 ? 0 : width;
        if(width==this.width){
            return this;
        }
        this.width = width;
        return this;
    }

    public DrawableComponent setHeight(int height) {        
        height = height < 0 ? 0 : height;
        if(height==this.height){
            return this;
        }
        this.height = height;
        return this;
    }
    
    public DrawableComponent setBgColor(Color bgColor) {
        this.bgColor = bgColor;
        return this;
    }

    public DrawableComponent setColor(Color color) {
        this.color = color;
        return this;
    }
        
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
