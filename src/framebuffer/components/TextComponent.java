package framebuffer.components;

import framebuffer.mid.Core;
import framebuffer.mid.Font;
import framebuffer.mid.Color;
import framebuffer.mid.FontException;

/**
 *
 * @author tokazio
 */
public abstract class TextComponent extends DrawableComponent {

    /**
     *
     */
    protected Font font;

    /**
     *
     */
    protected String caption = "Button";

    /**
     *
     */
    protected Color textColor = Color.WHITE;

    /**
     *
     */
    private int text_width;

    /**
     *
     */
    private int text_height;

    /**
     *
     * @param x
     * @param y
     * @param caption
     * @throws FontException
     */
    public TextComponent(int x, int y, String caption) throws FontException {
        super(x, y);
        Font f = new Font("/home/volumio/framebuffertest-c/Roboto-Thin.ttf", 22);
        setFont(f);
        setCaption(caption);
        width = text_width;
        height = text_height;
        System.out.println("text component ok");
    }

    /**
     *
     * @param font
     * @return
     */
    public final TextComponent setFont(Font font) {
        if (font == null || font.getPointer() < 0) {
            return this;
        }
        this.font = font;
        return this;
    }

    /**
     *
     * @return
     */
    public Font getFont() {
        return this.font;
    }

    /**
     *
     * @param caption
     * @return
     * @throws FontException
     */
    public final TextComponent setCaption(String caption) throws FontException {
        if (this.caption.equals(caption)) {
            return this;
        }
        this.caption = caption;
        calcTextSize();
        return this;
    }

    /**
     *
     * @throws FontException
     */
    private void calcTextSize() throws FontException {
        if (caption.isEmpty()) {
            this.text_width = 0;
            this.text_height = Core.text_height(font, "qQ");
        }
        if (font != null && font.getPointer() > 0) {
            this.text_width = Core.text_width(font, caption);
            this.text_height = Core.text_height(font, caption);
        }
    }

    /**
     *
     * @param textColor
     * @return
     */
    public TextComponent setTextColor(Color textColor) {
        this.textColor = textColor;
        return this;
    }

    /**
     * Halign
     * @return 
     */
    protected int calcTextX() {
        //center
        return (width - text_width) / 2;
    }

    /**
     * Valign
     * @return 
     */
    protected int calcTextY() {
        //middle
        return (height - text_height) / 2;
    }

}
