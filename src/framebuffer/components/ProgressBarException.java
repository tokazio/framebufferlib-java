package framebuffer.components;

/**
 *
 * @author tokazio
 */
public class ProgressBarException extends Exception {

    /**
     * 
     * @param s
     */
    public ProgressBarException(String s) {
        super(s);
    }
    
}
