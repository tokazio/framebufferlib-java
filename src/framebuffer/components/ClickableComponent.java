package framebuffer.components;

import framebuffer.mid.Clickable;
import framebuffer.mid.FontException;

/**
 *
 * @author tokazio
 */
public abstract class ClickableComponent extends TextComponent implements Clickable{

    public ClickableComponent(int x, int y, String caption) throws FontException {
        super(x, y, caption);
    }
    
}
