package framebuffer.components;

import framebuffer.mid.Core;
import framebuffer.low.FrameBufferLib;
import framebuffer.mid.ColorException;

/**
 *
 * @author tokazio
 */
public class ProgressBar extends DrawableComponent{

    private int position = 0;
    
    private float _p = 0;
    private float _w = 0;
    
    private int min = 0;
    private int max = 1;

    public ProgressBar(int x, int y, int w, int h) {
        super(x, y, w, h);
    }

    @Override
    public void staticDraw() throws ColorException{
        Core.fill_circle(x, y+ height/2, height/2, bgColor);        
        Core.fill_rect(x, y, width, height, bgColor);
        Core.fill_circle(x+width, y+ height/2, height/2, bgColor);
    }
    
    //Toutes les 25ms
    private float inc(){
        return ((((float)width/(float)(max-min))/1000)*25);
    }
    
    @Override
    public void draw() throws ColorException {
        if (_w < _p * width) {
            _w+=inc();
        }
        Core.fill_circle(x, y+ height/2, height/2, color);
        Core.fill_rect(x, y, (int)_w, height, color);
        Core.fill_circle(x+(int)_w, y+ height/2, height/2, color);
    }

    public void setPosition(int position) {
        position = position < min ? min : position;
        position = position > max ? max : position;
        this.position = position;
        _p = position / (float) (max - min);
    }

    public void setMin(int min) throws ProgressBarException {
        if (min >= max) {
            throw new ProgressBarException("La valeure min de la progressBar doit être inférieure à la valeure max.");
        }
        this.min = min;
    }

    public void setMax(int max) throws ProgressBarException {
        if (max <= min) {
            throw new ProgressBarException("La valeure max de la progressBar doit être supérieure à la valeure min.");
        }
        this.max = max;
    }

    public int getPosition() {
        return position;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
    
}
