package framebuffer.low;

import static com.sun.jna.Native.register;

/**
 *
 * @author tokazio
 */
public class FrameBufferLib {

    /**
     *
     */
    public static boolean isLoaded = false;

    /**
     *
     */
    static {
        try {
            register("framebuffer");
            isLoaded = true;
        } catch (UnsatisfiedLinkError ex) {
            System.out.println("framebuffer: " + ex.getClass().getName() + ": " + ex.getMessage());
        }
    }

    //rgba
    public static native long mapRgba(short r, short g, short b, short a);

    public static native long mapRgb(short r, short g, short b);

    public static native void unmapRgba(long col);

    //framebuffer
    public static native int fb_open(String fbFile);

    public static native void fb_close();

    public static native void fb_print_info();

    public static native void fb_hide_cursor();

    public static native void fb_show_cursor();

    public static native void fb_page_flip();

    public static native int fb_screen_width();

    public static native int fb_screen_height();

    public static native int fb_pixel_size();

    public static native int fb_line_size();

    public static native int fb_bpp();

    public static native int fb_page_size();

    public static native int fb_page_start();

    public static native int fb_xoffset();

    public static native int fb_yoffset();

    //draw
    public static native void fb_draw_pixel(int x, int y, long c);

    public static native void fb_fill_rect(int x, int y, int w, int h, long c);

    public static native void fb_clear_screen(long c);

    public static native void fb_fill_circle(int x0, int y0, int r, long c);

    public static native void fb_stroke_circle(int x0, int y0, int r, long c);

    public static native void fb_stroke_line(int x0, int y0, int x1, int y1, long c);

    public static native void fb_fill(int x, int y, long c, long s);

    //jpeg
    public static native long fb_jpg_image_load_file(String name);

    public static native void fb_fill_jpg_at(long image, int x0, int y0);

    public static native void fb_fill_jpg_at_size(long image, int x0, int y0, int width, int height);

    public static native void fb_fill_jpg_at_scale(long image, int x0, int y0, double xscale, double yscale);

    public static native void fb_jpg_image_(long image);

    //text
    public static native void text_open();

    public static native void text_close();

    public static native long text_open_font(String filename);

    public static native void text_close_font(long font);

    public static native void text_fill(long font, int x, int y, int point, String text, long col);

    public static native int text_height(long font, int point, String text);
    
    public static native int text_width(long font, int point, String text);

}
